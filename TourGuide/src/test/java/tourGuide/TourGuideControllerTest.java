package tourGuide;

import ch.qos.logback.core.net.ObjectWriter;
import com.fasterxml.jackson.databind.ObjectMapper;
import gpsUtil.GpsUtil;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import rewardCentral.RewardCentral;
import tourGuide.DTO.LocationDTO;
import tourGuide.DTO.NearbyAttractionDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;
import tripPricer.TripPricer;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.*;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class TourGuideControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TourGuideService tourGuideService;

    @MockBean
    private UserService userService;

    @MockBean
    private GpsUtilService gpsUtilService;

    @Test
    public void indexTest() throws Exception {
        mockMvc.perform(get("/"))
                .andExpect(status().isOk())
                .andExpect(content().string("Greetings from TourGuide!"));
    }

    @Test
    public void getLocationTest() throws Exception {
        String userName = "testUser";
        User user = new User(UUID.randomUUID(), userName, "000", "test@tourGuide.com");
        Location location = new Location(1.0, 1.0);
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), location, new Date());

        when(userService.getUser(userName)).thenReturn(user);
        when(tourGuideService.getUserLocation(user)).thenReturn(visitedLocation);

        mockMvc.perform(get("/getLocation").param("userName", userName))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(location)));
    }

    @Test
    public void getNearbyAttractionsTest() throws Exception {
        String userName = "testUser";
        User user = new User(UUID.randomUUID(), userName, "000", "test@tourGuide.com");
        VisitedLocation visitedLocation = new VisitedLocation(user.getUserId(), new Location(1.0, 1.0), new Date());
        NearbyAttractionDto nearbyAttractionDto = new NearbyAttractionDto(new ArrayList<>(), new ArrayList<>(), new LocationDTO(1.0, 1.0), new ArrayList<>(), new ArrayList<>());

        when(userService.getUser(userName)).thenReturn(user);
        when(tourGuideService.getUserLocation(user)).thenReturn(visitedLocation);
        when(tourGuideService.getNearByAttractions(visitedLocation, user)).thenReturn(nearbyAttractionDto);

        mockMvc.perform(get("/getNearbyAttractions").param("userName", userName))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(nearbyAttractionDto)));
    }

    @Test
    public void getRewardsTest() throws Exception {
        String userName = "testUser";
        User user = new User(UUID.randomUUID(), userName, "000", "test@tourGuide.com");
        List<UserReward> userRewards = new ArrayList<>();

        when(userService.getUser(userName)).thenReturn(user);
        when(userService.getUserRewards(user)).thenReturn(userRewards);

        mockMvc.perform(get("/getRewards").param("userName", userName))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(userRewards)));
    }

    @Test
    public void getAllCurrentLocationsTest() throws Exception {
        Map<String, LocationDTO> locations = new HashMap<>();

        when(userService.allCurrentLocations()).thenReturn(locations);

        mockMvc.perform(get("/getAllCurrentLocations"))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(locations)));
    }

    @Test
    public void getTripDealsTest() throws Exception {
        String userName = "testUser";
        User user = new User(UUID.randomUUID(), userName, "000", "test@tourGuide.com");
        List<Provider> providers = new ArrayList<>();

        when(userService.getUser(userName)).thenReturn(user);
        when(tourGuideService.getTripDeals(user)).thenReturn(providers);

        mockMvc.perform(get("/getTripDeals").param("userName", userName))
                .andExpect(status().isOk())
                .andExpect(content().json(new ObjectMapper().writeValueAsString(providers)));
    }
}
