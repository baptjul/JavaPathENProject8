package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;
import tourGuide.user.UserReward;
import tripPricer.TripPricer;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@TestPropertySource(properties = {"skip.tracker= false"})
public class RewardsServiceTest {

    @Autowired
    private Environment env;

    private GpsUtilService gpsUtilService;
    private RewardsService rewardsService;
    private UserService userService;
    private TourGuideService tourGuideService;

    @BeforeEach
    public void setUp() {
        GpsUtil gpsUtil = new GpsUtil();
        TripPricer tripPricer = new TripPricer();

        UserRepository userRepository = new UserRepository();
        AttractionRepository attractionRepository = new AttractionRepository(gpsUtil);

        gpsUtilService = new GpsUtilService(gpsUtil, attractionRepository);

        InternalTestHelper.setInternalUserNumber(1);
        userService = new UserService(gpsUtilService, userRepository);
        rewardsService = new RewardsService(gpsUtilService, new RewardCentral(), userService);
        boolean skipTracker = Boolean.parseBoolean(env.getProperty("skip.tracker", "true"));

        tourGuideService = new TourGuideService(gpsUtilService, new TripPricerService(tripPricer), rewardsService, userService, skipTracker);
    }

    @Test
    public void userGetRewards() throws ExecutionException, InterruptedException {
        List<User> allUsers = userService.getAllUsers();
        User user = allUsers.get(0);
        List<Attraction> attractions = gpsUtilService.getAttractions();
        user.setVisitedLocations(new ArrayList<>());
        user.setUserRewards(new ArrayList<>());

        attractions.forEach(attraction -> {
                    Location location = new Location(attraction.latitude, attraction.longitude);
                    user.addToVisitedLocations(new VisitedLocation(user.getUserId(), location, new Date()));
                }
        );

        rewardsService.calculateAndGetSingleUserRewards(user);

        List<UserReward> userRewards = user.getUserRewards();
        assertTrue(userRewards.size() > 0);
    }

    @Test
    public void isWithinAttractionProximity() {
        Attraction attraction = gpsUtilService.getAttractions().get(0);
        assertFalse(gpsUtilService.isWithinAttractionProximity(attraction, attraction));
    }

    @Test
    public void nearAllAttractions() throws ExecutionException, InterruptedException {
        gpsUtilService.setProximityBuffer(Integer.MAX_VALUE);

        User user = userService.getAllUsers().get(0);

        List<Attraction> attractions = gpsUtilService.getAttractions();
        user.setVisitedLocations(new ArrayList<>());
        user.setUserRewards(new ArrayList<>());

        attractions.forEach(attraction -> {
            Location location = new Location(attraction.latitude, attraction.longitude);
                    user.addToVisitedLocations(new VisitedLocation(user.getUserId(), location, new Date()));
                }
        );

        List<UserReward> userRewards = rewardsService.calculateAndGetSingleUserRewards(user);

        assertEquals(attractions.size(), userRewards.size());
    }
}
