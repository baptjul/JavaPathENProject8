package tourGuide;

import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import rewardCentral.RewardCentral;
import tourGuide.DTO.NearbyAttractionDto;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;
import tripPricer.Provider;
import tripPricer.TripPricer;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestPropertySource(properties = {"skip.tracker= false"})
public class TourGuideServiceTest {

	@Autowired
	private Environment env;

	private UserService userService;
	private TourGuideService tourGuideService;

	@BeforeEach
	public void setUp() {
		GpsUtil gpsUtil = new GpsUtil();
		TripPricer tripPricer = new TripPricer();

		UserRepository userRepository = new UserRepository();
		AttractionRepository attractionRepository = new AttractionRepository(gpsUtil);

		GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil, attractionRepository);
		userService = new UserService(gpsUtilService, userRepository);
		RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral(), userService);

		boolean skipTracker = Boolean.parseBoolean(env.getProperty("skip.tracker", "true"));
		tourGuideService = new TourGuideService(gpsUtilService, new TripPricerService(tripPricer), rewardsService, userService, skipTracker);

		InternalTestHelper.setInternalUserNumber(0);
	}

    @Test
    public void getUserLocation() {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
        VisitedLocation visitedLocation = tourGuideService.trackSingleUserLocation(user);

        assertTrue(visitedLocation.userId.equals(user.getUserId()));
		assertEquals(visitedLocation.userId, user.getUserId());
	}

	@Test
	public void addUser() {
        User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		userService.addUser(user);
		userService.addUser(user2);

		User retrivedUser = userService.getUser(user.getUserName());
		User retrivedUser2 = userService.getUser(user2.getUserName());

		assertEquals(user, retrivedUser);
		assertEquals(user2, retrivedUser2);
	}

	@Test
	public void getAllUsers() {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		User user2 = new User(UUID.randomUUID(), "jon2", "000", "jon2@tourGuide.com");

		userService.addUser(user);
		userService.addUser(user2);

		List<User> allUsers = userService.getAllUsers();

		assertTrue(allUsers.contains(user));
		assertTrue(allUsers.contains(user2));
	}

	@Test
	public void trackUser() throws ExecutionException, InterruptedException {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");
		VisitedLocation visitedLocation = tourGuideService.trackSingleUserLocation(user);

		assertEquals(user.getUserId(), visitedLocation.userId);
	}

	@Test
	public void getNearbyAttractions() throws ExecutionException, InterruptedException {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		VisitedLocation visitedLocation = tourGuideService.trackSingleUserLocation(user);

		NearbyAttractionDto nearbyAttractions = tourGuideService.getNearByAttractions(visitedLocation, user);

		assertEquals(5, nearbyAttractions.getAttractionNames().size());
	}

	@Test
	public void getTripDeals() {
		User user = new User(UUID.randomUUID(), "jon", "000", "jon@tourGuide.com");

		List<Provider> providers = tourGuideService.getTripDeals(user);

		assertEquals(5, providers.size());
	}
}
