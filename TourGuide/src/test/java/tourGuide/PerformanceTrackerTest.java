package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.TestPropertySource;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;
import tripPricer.TripPricer;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
@TestPropertySource(properties = {"skip.tracker= false"})
public class PerformanceTrackerTest {

    @Autowired
    private Environment env;
    private UserService userService;
    private TourGuideService tourGuideService;
    private StopWatch stopWatch;

    @BeforeEach
    public void setUp() {
        GpsUtil gpsUtil = new GpsUtil();
        TripPricer tripPricer = new TripPricer();

        UserRepository userRepository = new UserRepository();
        AttractionRepository attractionRepository = new AttractionRepository(gpsUtil);

        GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil, attractionRepository);
        InternalTestHelper.setInternalUserNumber(100000);
        userService = new UserService(gpsUtilService, userRepository);
        RewardsService rewardsService = new RewardsService(gpsUtilService, new RewardCentral(), userService);

        boolean skipTracker = Boolean.parseBoolean(env.getProperty("skip.tracker", "true"));
        tourGuideService = new TourGuideService(gpsUtilService, new TripPricerService(tripPricer), rewardsService, userService, skipTracker);

        stopWatch = new StopWatch();
    }

    @Test
    public void highVolumeTrackLocation() {
        List<User> allUsers = userService.getAllUsers();

        stopWatch.start();

        List<VisitedLocation> visitedLocations = tourGuideService.trackUsersLocations(allUsers);

        stopWatch.stop();
        tourGuideService.tracker.stopTracking();
        System.out.println("highVolumeTrackLocation: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(15) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
        assertEquals(visitedLocations.size(), InternalTestHelper.getInternalUserNumber());
    }
}
