package tourGuide;

import gpsUtil.GpsUtil;

import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.javamoney.moneta.Money;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import tourGuide.DTO.LocationDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;

import javax.money.CurrencyUnit;
import javax.money.Monetary;
import java.util.Date;
import java.util.Map;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
public class UserServiceTest {

    private UserService userService;

    @BeforeEach
    public void setUp() {
        GpsUtil gpsUtil = new GpsUtil();

        UserRepository userRepository = new UserRepository();
        AttractionRepository attractionRepository = new AttractionRepository(gpsUtil);

        GpsUtilService gpsUtilService = new GpsUtilService(gpsUtil, attractionRepository);
        InternalTestHelper.setInternalUserNumber(0);
        userService = new UserService(gpsUtilService, userRepository);
    }

    @Test
    public void updateUserPreferencesTest() {
        User user = new User(UUID.randomUUID(), "testUser", "000", "test@tourGuide.com");
        userService.addUser(user);
        CurrencyUnit currency = Monetary.getCurrency("USD");

        UserPreferences newPreferences = new UserPreferences();
        newPreferences.setAttractionProximity(100);
        newPreferences.setTripDuration(5);
        newPreferences.setCurrency(currency);
        newPreferences.setLowerPricePoint(Money.of(0, currency));
        newPreferences.setHighPricePoint( Money.of(Integer.MAX_VALUE, currency));
        newPreferences.setTicketQuantity(2);
        newPreferences.setNumberOfAdults(1);
        newPreferences.setNumberOfChildren(1);

        UserPreferences updatedPreferences = userService.updateUserPreferences(user.getUserName(), newPreferences);
        UserPreferences userPreferences = user.getUserPreferences();

        assertEquals(userPreferences, updatedPreferences);
        assertEquals(newPreferences.getAttractionProximity(), user.getUserPreferences().getAttractionProximity());
    }

    @Test
    public void allCurrentLocationsTest() {
        User user1 = new User(UUID.randomUUID(), "testUser1", "000", "test1@tourGuide.com");
        User user2 = new User(UUID.randomUUID(), "testUser2", "000", "test2@tourGuide.com");
        userService.addUser(user1);
        userService.addUser(user2);

        Location loc1 = new Location(1.0, 1.0);
        Location loc2 = new Location(2.0, 2.0);
        user1.addToVisitedLocations(new VisitedLocation(user1.getUserId(), loc1, new Date()));
        user2.addToVisitedLocations(new VisitedLocation(user2.getUserId(), loc2, new Date()));

        Map<String, LocationDTO> locations = userService.allCurrentLocations();

        assertTrue(locations.size() > 0);
    }
}

