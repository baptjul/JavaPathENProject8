package tourGuide;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.apache.commons.lang3.time.StopWatch;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import rewardCentral.RewardCentral;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.AttractionRepository;
import tourGuide.repository.UserRepository;
import tourGuide.service.*;
import tourGuide.user.User;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertTrue;

@SpringBootTest
public class PerformanceRewardsTest {

    private GpsUtilService gpsUtilService;
    private UserService userService;
    private RewardsService rewardsService;
    private StopWatch stopWatch;

    @BeforeEach
    public void setUp() {
        GpsUtil gpsUtil = new GpsUtil();

        UserRepository userRepository = new UserRepository();
        AttractionRepository attractionRepository = new AttractionRepository(gpsUtil);

        gpsUtilService = new GpsUtilService(gpsUtil, attractionRepository);
        InternalTestHelper.setInternalUserNumber(100000);
        userService = new UserService(gpsUtilService, userRepository);
        rewardsService = new RewardsService(gpsUtilService, new RewardCentral(), userService);

        stopWatch = new StopWatch();
    }

    @Test
    public void highVolumeGetRewards() throws ExecutionException, InterruptedException {
        List<User> allUsers = userService.getAllUsers();
        List<Attraction> attractions = gpsUtilService.getAttractions();
        Attraction attraction = attractions.get(0);
        allUsers.forEach(u -> u.addToVisitedLocations(new VisitedLocation(u.getUserId(), attraction, new Date())));

        stopWatch.start();

        rewardsService.calculateAndGetUserRewards(allUsers);

        stopWatch.stop();
        System.out.println("highVolumeGetRewards: Time Elapsed: " + TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()) + " seconds.");
        assertTrue(TimeUnit.MINUTES.toSeconds(20) >= TimeUnit.MILLISECONDS.toSeconds(stopWatch.getTime()));
        assertTrue(allUsers.stream().allMatch(u -> u.getUserRewards().size() > 0));
    }
}
