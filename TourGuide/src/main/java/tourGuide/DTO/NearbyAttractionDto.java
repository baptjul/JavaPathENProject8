package tourGuide.DTO;

import java.util.List;

/**
 * This class provides a model for nearby attraction data transfer objects (DTO).
 * It contains information about nearby attractions.
 */
public class NearbyAttractionDto {

    private List<String> attractionNames;
    private List<LocationDTO> attractionLocation;
    private LocationDTO userLocation;
    private List<Double> distances;
    private List<Integer> rewardPoints;

    /**
     * The constructor for the NearbyAttractionDto class.
     *
     * @param attractionNames    names of the nearby attractions
     * @param attractionLatLongs locations of the nearby attractions
     * @param userLatLong        location of the user
     * @param distances          distances from the user to the attractions
     * @param rewardPoints       reward points offered by the attractions
     */
    public NearbyAttractionDto(List<String> attractionNames, List<LocationDTO> attractionLatLongs, LocationDTO userLatLong, List<Double> distances, List<Integer> rewardPoints) {
        this.attractionNames = attractionNames;
        this.attractionLocation = attractionLatLongs;
        this.userLocation = userLatLong;
        this.distances = distances;
        this.rewardPoints = rewardPoints;
    }

    /**
     * Returns the names of the attractions.
     *
     * @return the names of the attractions
     */
    public List<String> getAttractionNames() {
        return attractionNames;
    }

    /**
     * Sets the names of the attractions.
     *
     * @param attractionNames the names of the attractions
     */
    public void setAttractionNames(List<String> attractionNames) {
        this.attractionNames = attractionNames;
    }

    /**
     * Returns the locations of the attractions.
     *
     * @return the locations of the attractions
     */
    public List<LocationDTO> getAttractionLocation() {
        return attractionLocation;
    }

    /**
     * Sets the locations of the attractions.
     *
     * @param attractionLocation the locations of the attractions
     */
    public void setAttractionLocation(List<LocationDTO> attractionLocation) {
        this.attractionLocation = attractionLocation;
    }

    /**
     * Returns the location of the user.
     *
     * @return the location of the user
     */
    public LocationDTO getUserLocation() {
        return userLocation;
    }

    /**
     * Sets the location of the user.
     *
     * @param userLocation the location of the user
     */
    public void setUserLocation(LocationDTO userLocation) {
        this.userLocation = userLocation;
    }

    /**
     * Returns the distances from the user to the attractions.
     *
     * @return the distances from the user to the attractions
     */
    public List<Double> getDistances() {
        return distances;
    }

    /**
     * Sets the distances from the user to the attractions.
     *
     * @param distances the distances from the user to the attractions
     */
    public void setDistances(List<Double> distances) {
        this.distances = distances;
    }

    /**
     * Returns the reward points offered by the attractions.
     *
     * @return the reward points offered by the attractions
     */
    public List<Integer> getRewardPoints() {
        return rewardPoints;
    }

    /**
     * Sets the reward points offered by the attractions.
     *
     * @param rewardPoints the reward points offered by the attractions
     */
    public void setRewardPoints(List<Integer> rewardPoints) {
        this.rewardPoints = rewardPoints;
    }
}
