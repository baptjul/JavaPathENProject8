package tourGuide.DTO;

/**
 * This class represents a location data transfer object (DTO).
 * It contains the latitude and longitude coordinates of a location.
 */
public class LocationDTO {
    private double longitude;
    private double latitude;

    /**
     * The constructor for the LocationDTO class.
     *
     * @param longitude the longitude of the location
     * @param latitude  the latitude of the location
     */
    public LocationDTO(double longitude, double latitude) {
        this.longitude = longitude;
        this.latitude = latitude;
    }

    /**
     * Returns the longitude of the location.
     *
     * @return the longitude of the location
     */
    public double getLongitude() {
        return longitude;
    }

    /**
     * Sets the longitude of the location.
     *
     * @param longitude the longitude of the location
     */
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    /**
     * Returns the latitude of the location.
     *
     * @return the latitude of the location
     */
    public double getLatitude() {
        return latitude;
    }

    /**
     * Sets the latitude of the location.
     *
     * @param latitude the latitude of the location
     */
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }
}
