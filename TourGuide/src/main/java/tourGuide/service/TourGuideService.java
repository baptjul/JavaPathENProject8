package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import tourGuide.DTO.LocationDTO;
import tourGuide.DTO.NearbyAttractionDto;
import tourGuide.tracker.Tracker;
import tourGuide.user.User;
import tourGuide.user.UserReward;
import tripPricer.Provider;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *  Service for the Tour Guide.
 */
@Service
public class TourGuideService {

    final boolean testMode = true;
    private final Logger logger = LoggerFactory.getLogger(TourGuideService.class);
    private final RewardsService rewardsService;
    private final GpsUtilService gpsUtilService;
    private final TripPricerService tripPricerService;
    private final UserService userService;
    private final ExecutorService executorService = Executors.newFixedThreadPool(1000);
    public Tracker tracker;


    /**
     * Constructor for TourGuideService.
     *
     * @param gpsUtilService    the GpsUtilService instance
     * @param tripPricerService the TripPricerService instance
     * @param rewardsService    the RewardsService instance
     * @param userService       the UserService instance
     * @param skipTracker       a boolean to skip the tracker if it is true
     */
    public TourGuideService(GpsUtilService gpsUtilService, TripPricerService tripPricerService, RewardsService rewardsService, UserService userService, @Value("${skip.tracker:false}") boolean skipTracker) {
        this.gpsUtilService = gpsUtilService;
        this.rewardsService = rewardsService;
        this.tripPricerService = tripPricerService;
        this.userService = userService;

        if (testMode) {
            Logger logger = LoggerFactory.getLogger(TourGuideService.class);
            logger.info("TestMode enabled");
            logger.debug("Initializing users");
            userService.initializeInternalUsers();
            logger.debug("Finished initializing users");
        }

        if (!skipTracker) {
            tracker = new Tracker(this, userService);
            addShutDownHook();
        }
    }

    /**
     * Tracks the location of a particular user.
     *
     * @param user the User instance
     * @return a CompletableFuture of VisitedLocation
     */
    public CompletableFuture<VisitedLocation> trackUserLocation(User user) {
        logger.debug("Tracking user location for {}", user.getUserName());
        return CompletableFuture.supplyAsync(() -> {
            VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());
            user.addToVisitedLocations(visitedLocation);

            return visitedLocation;
        }, executorService).thenApplyAsync((visitedLocation) -> {
            List<Attraction> attractions = gpsUtilService.getAttractions();
            attractions.forEach(attraction -> {
                if (gpsUtilService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
                    user.addUserReward(new UserReward(visitedLocation, attraction, rewardsService.getRewardPoints(attraction, user)));
                }
            });
            return visitedLocation;
        }, executorService);
    }

    /**
     * Tracks the location of a single user.
     *
     * @param user the User instance
     * @return the VisitedLocation instance
     */
    public VisitedLocation trackSingleUserLocation(User user) {
        logger.debug("Tracking single user location for {}", user.getUserName());
        return trackUserLocation(user).join();
    }

    /**
     * Tracks the locations of multiple users.
     *
     * @param users a list of User instances
     * @return a list of VisitedLocation instances
     */
    public List<VisitedLocation> trackUsersLocations(List<User> users) {
        logger.debug("Tracking locations for multiple users");
        List<CompletableFuture<VisitedLocation>> allUsersLocationsFutures =
                users.stream().map(user ->
                                CompletableFuture.supplyAsync(() -> {
                                    VisitedLocation visitedLocation = gpsUtilService.getUserLocation(user.getUserId());
                                    user.addToVisitedLocations(visitedLocation);
                                    return visitedLocation;
                                }, executorService))
                        .toList();

        CompletableFuture.allOf(allUsersLocationsFutures.toArray(new CompletableFuture[0])).join();

        return allUsersLocationsFutures.stream().map(CompletableFuture::join).toList();
    }

    /**
     * Gets the location of a particular user.
     *
     * @param user the User instance
     * @return the VisitedLocation instance
     */
    public VisitedLocation getUserLocation(User user) {
        logger.debug("Getting location for user {}", user.getUserName());
        return user.getVisitedLocations().isEmpty() ? this.trackSingleUserLocation(user) :
                user.getLastVisitedLocation();
    }

    /**
     * Gets the nearby attractions for a visited location.
     *
     * @param visitedLocation the VisitedLocation instance
     * @param user            the User instance
     * @return a NearbyAttractionDto instance
     */
    public NearbyAttractionDto getNearByAttractions(VisitedLocation visitedLocation, User user) {
        logger.debug("Getting nearby attractions for user {}", user.getUserName());
        LocationDTO userPosition = new LocationDTO(visitedLocation.location.longitude, visitedLocation.location.latitude);

        Map<Double, Attraction> distancesToAttractions = new TreeMap<>();
        gpsUtilService.getAttractions().forEach(attraction -> {
            if (gpsUtilService.isWithinAttractionProximity(attraction, visitedLocation.location)) {
                double distance = gpsUtilService.getDistance(visitedLocation.location, attraction);
                distancesToAttractions.put(distance, attraction);
            }
        });

        List<Attraction> nearbyAttractions = new ArrayList<>();
        List<String> attractionNames = new ArrayList<>();
        List<LocationDTO> attractionLocation = new ArrayList<>();
        List<Double> distances = new ArrayList<>();
        List<Integer> rewardPoints = new ArrayList<>();

        for (Map.Entry<Double, Attraction> attraction : distancesToAttractions.entrySet()) {
            if (nearbyAttractions.size() >= 5) {
                break;
            }
            distances.add(attraction.getKey());
            nearbyAttractions.add(attraction.getValue());
            attractionNames.add(attraction.getValue().attractionName);
            attractionLocation.add(new LocationDTO(attraction.getValue().longitude, attraction.getValue().latitude));
            rewardPoints.add(rewardsService.getRewardPoints(attraction.getValue(), user));
        }

        return new NearbyAttractionDto(attractionNames, attractionLocation, userPosition, distances, rewardPoints);
    }

    /**
     * Gets the trip deals for a particular user.
     *
     * @param user the User instance
     * @return a list of Provider instances
     */
    public List<Provider> getTripDeals(User user) {
        logger.debug("Getting trip deals for user {}", user.getUserName());
        int cumulativeRewardPoints = user.getUserRewards().stream().mapToInt(UserReward::getRewardPoints).sum();

        List<Provider> providers = tripPricerService.getPrice(
                user,
                user.getUserId(),
                user.getUserPreferences().getNumberOfAdults(),
                user.getUserPreferences().getNumberOfChildren(),
                user.getUserPreferences().getTripDuration(),
                cumulativeRewardPoints
        );

        user.setTripDeals(providers);
        return providers;
    }

    private void addShutDownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread(tracker::stopTracking));
    }
}
