package tourGuide.service;

import gpsUtil.location.Attraction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import rewardCentral.RewardCentral;
import tourGuide.user.User;
import tourGuide.user.UserReward;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 *  Service to manage rewards.
 */
@Service
public class RewardsService {

    private final Logger logger = LoggerFactory.getLogger(RewardsService.class);

    private final RewardCentral rewardsCentral;
    private final GpsUtilService gpsUtilService;
    private final UserService userService;
    private final ExecutorService executorService = Executors.newFixedThreadPool(800);


    /**
     * Constructor for RewardsService.
     *
     * @param gpsUtilService the GpsUtilService instance
     * @param rewardCentral  the RewardCentral instance
     * @param userService    the UserService instance
     */
    public RewardsService(GpsUtilService gpsUtilService, RewardCentral rewardCentral, UserService userService) {
        this.gpsUtilService = gpsUtilService;
        this.rewardsCentral = rewardCentral;
        this.userService = userService;
    }

    protected void singleThreadCalculateAndGetUserRewards(User user, List<Attraction> attractions) {
        logger.debug("Calculating rewards for user {}", user.getUserName());
        user.getVisitedLocations().forEach(visitedLocation ->
                attractions.forEach(attraction -> {
                    if ((gpsUtilService.nearAttraction(visitedLocation, attraction))
                            && (!userService.isDuplicateReward(user, attraction))) {
                        UserReward newReward = new UserReward(visitedLocation, attraction,
                                rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId()));
                        user.addUserReward(newReward);
                    }
                })
        );
    }

    /**
     * Calculate and get rewards for all users.
     *
     * @param users the list of users
     * @throws InterruptedException, ExecutionException
     */
    public void calculateAndGetUserRewards(List<User> users) throws InterruptedException, ExecutionException {
        logger.info("Calculating rewards for all users");
        Thread.sleep(5000);
        List<Attraction> attractions = gpsUtilService.getAttractions();

        List<CompletableFuture<Void>> allUsersRewardsFutures =
                users.stream().map(user ->
                                CompletableFuture.runAsync(() ->
                                        singleThreadCalculateAndGetUserRewards(user, attractions), executorService))
                        .toList();

        CompletableFuture.allOf(allUsersRewardsFutures.toArray(new CompletableFuture[0])).get();
    }

    /**
     * Calculate and get rewards for a single user.
     *
     * @param user the user
     * @return list of UserReward
     * @throws InterruptedException, ExecutionException
     */
    public List<UserReward> calculateAndGetSingleUserRewards(User user) throws InterruptedException, ExecutionException {
        logger.debug("Calculating rewards for single user {}", user.getUserName());
        List<Attraction> attractions = gpsUtilService.getAttractions();
        List<CompletableFuture<UserReward>> completableFutureList = new ArrayList<>();

        user.getVisitedLocations().forEach(visitedLocation -> {
            attractions.stream()
                    .filter(attraction -> attraction.latitude == visitedLocation.location.latitude
                            && attraction.longitude == visitedLocation.location.longitude)
                    .forEach(attraction -> {
                        if (user.getUserRewards().stream().noneMatch(r -> r.attraction.attractionName.equals(attraction.attractionName))) {
                            CompletableFuture<UserReward> completableFuture = CompletableFuture.supplyAsync(() -> new UserReward(visitedLocation, attraction, getRewardPoints(attraction, user)));
                            completableFutureList.add(completableFuture);
                        }
                    });
        });

        CompletableFuture.allOf(completableFutureList.toArray(new CompletableFuture[0]))
                .thenAccept(f -> completableFutureList.forEach(
                        userRewardsFuture -> user.addUserReward(userRewardsFuture.join()))).get();

        List<UserReward> userRewards = user.getUserRewards();
        List<UserReward> userRewardsNoDuplicates = new ArrayList<>();
        for (UserReward userReward : userRewards) {
            if (userRewardsNoDuplicates.stream().noneMatch(r -> r.attraction.attractionName.equals(userReward.attraction.attractionName))) {
                userRewardsNoDuplicates.add(userReward);
            }
        }

        return userRewardsNoDuplicates;
    }

    /**
     * Get reward points for a specific user and attraction.
     *
     * @param attraction the attraction
     * @param user       the user
     * @return the reward points
     */
    public int getRewardPoints(Attraction attraction, User user) {
        logger.debug("Getting reward points for user {} and attraction {}", user.getUserName(), attraction.attractionName);
        return rewardsCentral.getAttractionRewardPoints(attraction.attractionId, user.getUserId());
    }
}
