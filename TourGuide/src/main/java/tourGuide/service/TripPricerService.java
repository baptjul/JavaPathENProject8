package tourGuide.service;

import org.javamoney.moneta.Money;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.user.User;
import tripPricer.Provider;
import tripPricer.TripPricer;

import javax.money.CurrencyUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Service for trip pricing.
 */
@Service
public class TripPricerService {

    private static final String tripPricerApiKey = "test-server-api-key";
    private final Logger logger = LoggerFactory.getLogger(TripPricerService.class);
    private final TripPricer tripPricer;

    /**
     * Constructor for TripPricerService
     *
     * @param tripPricer the TripPricer instance
     */
    public TripPricerService(TripPricer tripPricer) {
        this.tripPricer = tripPricer;
    }


    /**
     * Fetches the trip prices for a particular user based on their preferences.
     *
     * @param user          the User instance
     * @param userId        the UUID of the user
     * @param adults        number of adults
     * @param children      number of children
     * @param nightsStay    number of nights of stay
     * @param rewardsPoints number of reward points
     * @return a list of Providers matching the user preferences
     */
    public List<Provider> getPrice(User user, UUID userId, int adults, int children, int nightsStay, int rewardsPoints) {
        logger.debug("Fetching trip prices for user {}", user.getUserName());
        CurrencyUnit userCurrency = user.getUserPreferences().getCurrency();
        Money lowerPricePoint = user.getUserPreferences().getLowerPricePoint();
        Money highPricePoint = user.getUserPreferences().getHighPricePoint();
        int ticketQuantity = user.getUserPreferences().getTicketQuantity();

        List<Provider> providers = tripPricer.getPrice(
                tripPricerApiKey,
                userId,
                adults,
                children,
                nightsStay,
                rewardsPoints
        );

        // check quantity
        List<Provider> updatedProviders = new ArrayList<>();
        for (Provider provider : providers) {
            double updatedPrice = provider.price * ticketQuantity;
            Provider updatedProvider = new Provider(provider.tripId, provider.name, updatedPrice);
            updatedProviders.add(updatedProvider);
        }

        // check price range
        return updatedProviders.stream()
                .filter(provider -> {
                    Money priceInUserCurrency = Money.of(provider.price, userCurrency);
                    return priceInUserCurrency.isGreaterThanOrEqualTo(lowerPricePoint) &&
                            priceInUserCurrency.isLessThanOrEqualTo(highPricePoint);
                })
                .toList();
    }
}
