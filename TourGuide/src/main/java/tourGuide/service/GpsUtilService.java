package tourGuide.service;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.repository.AttractionRepository;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.*;

/**
 *  Service to handle localisations.
 */
@Service
public class GpsUtilService {

    private static final double STATUTE_MILES_PER_NAUTICAL_MILE = 1.15077945;
    private static GpsUtil gpsUtil;
    private final Logger logger = LoggerFactory.getLogger(GpsUtilService.class);
    private final int defaultProximityBuffer = 10;
    private final AttractionRepository attractionRepository;
    private int proximityBuffer = defaultProximityBuffer;

    /**
     * Constructor for GpsUtilService.
     *
     * @param gpsUtil              the GpsUtil instance
     * @param attractionRepository the AttractionRepository instance
     */
    public GpsUtilService(GpsUtil gpsUtil, AttractionRepository attractionRepository) {
        Locale.setDefault(Locale.ENGLISH);
        GpsUtilService.gpsUtil = gpsUtil;
        this.attractionRepository = attractionRepository;
    }

    /**
     * Get a list of all attractions.
     *
     * @return a list of all attractions
     */
    public List<Attraction> getAttractions() {
        logger.debug("Fetching all attractions");
        return attractionRepository.getAttractions();
    }

    /**
     * Get the current location of a user.
     *
     * @param userId the id of the user
     * @return the visited location of the user
     */
    public VisitedLocation getUserLocation(UUID userId) {
        logger.debug("Fetching location for user with id {}", userId);
        return gpsUtil.getUserLocation(userId);
    }

    /**
     * Generates a random longitude within the valid global range.
     *
     * @return random longitude
     */
    public double generateRandomLongitude() {
        logger.debug("Generating random longitude");
        double leftLimit = -180;
        double rightLimit = 180;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * Generates a random latitude within the valid global range.
     *
     * @return random latitude
     */
    public double generateRandomLatitude() {
        logger.debug("Generating random latitude");
        double leftLimit = -85.05112878;
        double rightLimit = 85.05112878;
        return leftLimit + new Random().nextDouble() * (rightLimit - leftLimit);
    }

    /**
     * Generates a random date within the last 30 days.
     *
     * @return random date
     */
    public Date getRandomTime() {
        logger.debug("Generating random time within the last 30 days");
        LocalDateTime localDateTime = LocalDateTime.now().minusDays(new Random().nextInt(30));
        return Date.from(localDateTime.toInstant(ZoneOffset.UTC));
    }

    /**
     * Sets the proximity buffer to a specified value.
     *
     * @param proximityBuffer the new proximity buffer value
     */
    public void setProximityBuffer(int proximityBuffer) {
        logger.info("Setting proximity buffer to {}", proximityBuffer);
        this.proximityBuffer = proximityBuffer;
    }

    /**
     * Resets the proximity buffer to the default value.
     */
    public void setDefaultProximityBuffer() {
        logger.info("Resetting proximity buffer to default");
        proximityBuffer = defaultProximityBuffer;
    }

    /**
     * Checks if a location is within a certain distance of an attraction.
     *
     * @param attraction the attraction to check
     * @param location   the location to check
     * @return true if within distance, false otherwise
     */
    public boolean isWithinAttractionProximity(Attraction attraction, Location location) {
        logger.debug("Checking if location is within proximity of attraction");
        return getDistance(attraction, location) > 200;
    }

    /**
     * Checks if a visited location is near an attraction.
     *
     * @param visitedLocation the visited location
     * @param attraction      the attraction
     * @return true if near, false otherwise
     */
    public boolean nearAttraction(VisitedLocation visitedLocation, Attraction attraction) {
        logger.debug("Checking if visited location is near attraction");
        return !(getDistance(attraction, visitedLocation.location) > proximityBuffer);
    }

    /**
     * Calculate the distance between a visited location and an attraction.
     *
     * @param visitedLocation the visited location
     * @param attraction      the attraction
     * @return the distance between the two points
     */
    public double getDistance(Location visitedLocation, Location attraction) {
        logger.debug("Calculating distance between visited location and attraction");
        double lat1 = Math.toRadians(visitedLocation.latitude);
        double lon1 = Math.toRadians(visitedLocation.longitude);
        double lat2 = Math.toRadians(attraction.latitude);
        double lon2 = Math.toRadians(attraction.longitude);

        double angle = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon1 - lon2));

        double nauticalMiles = 60 * Math.toDegrees(angle);
        return STATUTE_MILES_PER_NAUTICAL_MILE * nauticalMiles;
    }
}