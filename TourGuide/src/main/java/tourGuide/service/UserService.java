package tourGuide.service;

import gpsUtil.location.Attraction;
import gpsUtil.location.Location;
import gpsUtil.location.VisitedLocation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import tourGuide.DTO.LocationDTO;
import tourGuide.helper.InternalTestHelper;
import tourGuide.repository.UserRepository;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;

import java.util.*;
import java.util.stream.IntStream;

/**
 * Service for managing Users and their related functionality.
 */
@Service
public class UserService {
    private final Logger logger = LoggerFactory.getLogger(UserService.class);
    //private final GpsUtil gpsUtil;
    private final UserRepository userRepository;
    private final GpsUtilService gpsUtilService;

    /**
     * Create a new UserService.
     *
     * @param gpsUtilService the service to generate GPS and location data.
     * @param userRepository the repository to manage User data.
     */
    public UserService(GpsUtilService gpsUtilService, UserRepository userRepository) {
        logger.info("Initializing UserService.");
        this.gpsUtilService = gpsUtilService;
        this.userRepository = userRepository;
    }

    /**
     * Fetch a User by their username.
     *
     * @param userName the username of the desired User.
     * @return the User with the provided username, or null if no such User exists.
     */
    public User getUser(String userName) {
        logger.debug("Fetching user: {}", userName);
        return userRepository.getInternalUserMap().get(userName);
    }

    /**
     * Fetch all Users.
     *
     * @return a List containing all Users.
     */
    public List<User> getAllUsers() {
        logger.debug("Fetching all users.");
        return new ArrayList<>(userRepository.getInternalUserMap().values());
    }

    /**
     * Add a User to the repository.
     *
     * @param user the User to be added.
     */
    public void addUser(User user) {
        logger.debug("Adding user: {}", user.getUserName());
        if (!userRepository.getInternalUserMap().containsKey(user.getUserName())) {
            userRepository.addUserToInternalUserMap(user.getUserName(), user);
        }
    }

    /**
     * Update User's preferences based on the provided UserPreferences.
     *
     * @param userName        the name of the User whose preferences are to be updated.
     * @param userPreferences the new preferences of the User.
     * @return updated UserPreferences if the User is found, otherwise null.
     */
    public UserPreferences updateUserPreferences(String userName, UserPreferences userPreferences) {
        logger.debug("Updating preferences for user: {}", userName);
        User target = getUser(userName);
        if (target != null) {
            target.getUserPreferences().setAttractionProximity(userPreferences.getAttractionProximity());
            target.getUserPreferences().setCurrency(userPreferences.getCurrency());
            target.getUserPreferences().setLowerPricePoint(userPreferences.getLowerPricePoint());
            target.getUserPreferences().setHighPricePoint(userPreferences.getHighPricePoint());
            target.getUserPreferences().setTripDuration(userPreferences.getTripDuration());
            target.getUserPreferences().setTicketQuantity(userPreferences.getTicketQuantity());
            target.getUserPreferences().setNumberOfAdults(userPreferences.getNumberOfAdults());
            target.getUserPreferences().setNumberOfChildren(userPreferences.getNumberOfChildren());

            return target.getUserPreferences();
        } else {
            return null;
        }
    }

    /**
     * Fetch all rewards for the provided User.
     *
     * @param user the User whose rewards are to be fetched.
     * @return a List of UserReward for the provided User.
     */
    public List<UserReward> getUserRewards(User user) {
        logger.debug("Fetching rewards for user: {}", user.getUserName());
        return user.getUserRewards();
    }

    /**
     * Fetches the current locations of all users.
     *
     * @return a Map with User's UUID as key and their current location as value.
     */
    public Map<String, LocationDTO> allCurrentLocations() {
        logger.debug("Fetching all current locations");
        Map<String, LocationDTO> usersLocation = new TreeMap<>();
        List<User> users = getAllUsers();
        users.parallelStream().forEach(user -> {
            VisitedLocation lastVisitedLocation = user.getVisitedLocations().get(user.getVisitedLocations().size() - 1);
            Location lastLocation = lastVisitedLocation.location;
            usersLocation.put(user.getUserId().toString(), new LocationDTO(lastLocation.longitude, lastLocation.latitude));
        });

        return usersLocation;
    }

    /**
     * Checks if a user already has a reward for a particular attraction.
     *
     * @param user       the user to check for duplicate rewards.
     * @param attraction the attraction to check for duplicate rewards.
     * @return true if a duplicate reward is found, false otherwise.
     */
    public boolean isDuplicateReward(User user, Attraction attraction) {
        logger.debug("Checking for duplicate rewards for user: {}", user.getUserName());
        boolean isDuplicate = false;
        List<UserReward> userRewards = user.getUserRewards();

        for (UserReward userReward : userRewards) {
            if (userReward.attraction.attractionName.equals(attraction.attractionName)) {
                isDuplicate = true;
                break;
            }
        }

        return isDuplicate;
    }

    /**
     * Initializes internal users for testing.
     */
    public void initializeInternalUsers() {
        logger.debug("Initializing internal users");
        IntStream.range(0, InternalTestHelper.getInternalUserNumber()).forEach(i -> {
            String userName = "internalUser" + i;
            String phone = "000";
            String email = userName + "@tourGuide.com";
            User user = new User(UUID.randomUUID(), userName, phone, email);
            generateUserLocationHistory(user);

            userRepository.addUserToInternalUserMap(userName, user);//.put(userName, user);
        });
        logger.debug("Created " + InternalTestHelper.getInternalUserNumber() + " internal test users.");
    }

    /**
     * Generates location history for a given user.
     *
     * @param user the User for whom the location history is to be generated.
     */
    public void generateUserLocationHistory(User user) {
        logger.debug("Generating user location history for user: {}", user.getUserName());
        IntStream.range(0, 3).forEach(i -> {
            user.addToVisitedLocations(new VisitedLocation(user.getUserId(), new Location(gpsUtilService.generateRandomLatitude(), gpsUtilService.generateRandomLongitude()), gpsUtilService.getRandomTime()));
        });
    }
}
