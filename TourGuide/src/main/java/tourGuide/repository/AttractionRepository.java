package tourGuide.repository;

import gpsUtil.GpsUtil;
import gpsUtil.location.Attraction;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents a repository for attractions.
 * It provides access to the attraction's data.
 */
@Repository
public class AttractionRepository {

    private static GpsUtil gpsUtil;
    public static List<Attraction> attractions = new ArrayList<>();

    /**
     * Constructs a new AttractionRepository with the specified GpsUtil.
     *
     * @param gpsUtil the GpsUtil instance used to retrieve attractions
     */
    public AttractionRepository(GpsUtil gpsUtil) {
        AttractionRepository.gpsUtil = gpsUtil;
    }

    /**
     * Returns the list of attractions.
     * If the attractions list is empty, it retrieves the attractions from the GpsUtil.
     *
     * @return the list of attractions
     */
    public List<Attraction> getAttractions() {
        if (attractions.size() == 0) {
            attractions = gpsUtil.getAttractions();
        }
        return attractions;
    }

}
