package tourGuide.repository;

import org.springframework.stereotype.Repository;
import tourGuide.user.User;

import java.util.HashMap;
import java.util.Map;

/**
 * This class acts as the repository for the User.
 * It manages an internal map where users are stored and can be accessed.
 */
@Repository
public class UserRepository {
    public Map<String, User> internalUserMap = new HashMap<>();

    /**
     * This method returns the map containing all users.
     *
     * @return a Map object containing all users
     */
    public Map<String, User> getInternalUserMap() {
        return internalUserMap;
    }

    /**
     * This method adds a user to the internal user map.
     *
     * @param key a string key for accessing the user
     * @param user the user object to be added to the map
     */
    public void addUserToInternalUserMap(String key, User user) {
        internalUserMap.put(key, user);
    }
}