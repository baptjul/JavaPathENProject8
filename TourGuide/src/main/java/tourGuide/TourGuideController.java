package tourGuide;

import java.util.List;
import java.util.Map;
import gpsUtil.location.Location;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import gpsUtil.location.VisitedLocation;
import tourGuide.DTO.LocationDTO;
import tourGuide.DTO.NearbyAttractionDto;
import tourGuide.service.GpsUtilService;
import tourGuide.service.TourGuideService;
import tourGuide.service.UserService;
import tourGuide.user.User;
import tourGuide.user.UserPreferences;
import tourGuide.user.UserReward;
import tripPricer.Provider;

/**
 * The Controller class for the Tour Guide application.
 * It handles all the incoming HTTP requests to the server.
 */
@RestController
public class TourGuideController {

    private final Logger logger = LoggerFactory.getLogger(TourGuideController.class);

    @Autowired
	TourGuideService tourGuideService;
    @Autowired
    GpsUtilService gpsUtilService;
    @Autowired
    UserService userService;


    /**
     * The root URL request.
     * @return a greeting message
     */
    @RequestMapping("/")
    public String index() {
        logger.info("Greetings endpoint");
        return "Greetings from TourGuide!";
    }

    /**
     * Get a user's location.
     * @param userName the name of the user
     * @return the user's location
     */
    @RequestMapping("/getLocation")
    @ResponseBody
    public Location getLocation(@RequestParam String userName) {
        logger.info("Fetching location for user: {}", userName);
    	VisitedLocation visitedLocation = tourGuideService.getUserLocation(getUser(userName));
		return visitedLocation.location;
    }

    /**
     * Get nearby attractions for a user.
     * @param userName the name of the user
     * @return the nearby attractions
     */
    @RequestMapping("/getNearbyAttractions")
    @ResponseBody
    public NearbyAttractionDto getNearbyAttractions(@RequestParam String userName) {
        logger.info("Fetching nearby attractions for user: {}", userName);
        User user = userService.getUser(userName);
        VisitedLocation visitedLocation = tourGuideService.getUserLocation(user);
        return tourGuideService.getNearByAttractions(visitedLocation, getUser(userName));
    }

    /**
     * Get the rewards for a user.
     * @param userName the name of the user
     * @return the user's rewards
     */
    @RequestMapping("/getRewards")
    @ResponseBody
    public List<UserReward> getRewards(@RequestParam String userName) {
        logger.info("Fetching rewards for user: {}", userName);
    	return userService.getUserRewards(getUser(userName));
    }

    /**
     * Get all current locations of users.
     * @return the locations of all users
     */
    @RequestMapping("/getAllCurrentLocations")
    @ResponseBody
    public Map<String, LocationDTO> getAllCurrentLocations() {
        logger.info("Fetching all current user locations");
    	return userService.allCurrentLocations();
    }

    /**
     * Get trip deals for a user.
     * @param userName the name of the user
     * @return the trip deals for the user
     */
    @RequestMapping("/getTripDeals")
    @ResponseBody
    public List<Provider> getTripDeals(@RequestParam String userName) {
        logger.info("Fetching trip deals for user: {}", userName);
        return tourGuideService.getTripDeals(getUser(userName));
    }

    /**
     * Update a user's preferences.
     * @param userName the name of the user
     * @param userPreferences the user's new preferences
     * @return the updated user preferences
     */
    @RequestMapping("/preferences")
    public UserPreferences updateUserPreferences(@RequestParam  String userName, @RequestBody UserPreferences userPreferences) {
        logger.info("Updating preferences for user: {}", userName);
        return userService.updateUserPreferences(userName, userPreferences);
    }

    /**
     * Get a User object for a given username.
     * @param userName the name of the user
     * @return the User object for the user
     */
    private User getUser(String userName) {
        logger.info("Fetching user: {}", userName);
    	return userService.getUser(userName);
    }
}